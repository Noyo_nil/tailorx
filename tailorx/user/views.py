from django.shortcuts import render, redirect
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from user.models import User

# Create your views here.
def index(request):
    if not request.user.is_authenticated:
        return render(request, "login/login.html")
    else:
        return redirect('dashboard')

def dashboard(request):
    if not request.user.is_authenticated:
        return redirect('index')
    else:
        if request.user.is_superuser:
            return render(request, "dashboard.html")
        else:
            return render(request, 'dashboard.html')

#########################
## Login, Logout Error
#########################

def loginUser(request):
    if request.method == 'POST':
        if '@' in request.POST['username']:
            try:
                user = User.objects.get(email=request.POST['username'])
                if user.is_active == False:
                    message = 'Inactive user! Sorry you don\'t have permission to access Smart Tracker'
                    messages.error(request, message)
                    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
                username = user.username
            except User.DoesNotExist:
                message = 'User does not exist!'
                messages.error(request, message)
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        else:
            username=request.POST['username']

        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is None:
            message = 'User name / Password Mismatched!'
            messages.warning(request, message)
        else:
            login(request, user)
            message = 'Welcome to Smart Tracker!'
            messages.success(request, message)
        # return redirect('dashboard')
        return redirect('index')
    else:
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def logoutUser(request):
    logout(request)
    message = 'Successfully Logged Out!'
    messages.success(request, message)
    return redirect('index')

def error_404_view(request):
    return redirect('index')

def handler500(request, *args, **argv):
    return redirect('index')