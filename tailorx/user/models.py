from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone

class User(AbstractUser):
    name = models.CharField(max_length = 100, null=True)
    email = models.EmailField(unique=True)
    phone = models.CharField(max_length = 20, null=True)
    address = models.CharField(max_length = 50, null=True)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)

    class Meta:
        db_table = 'users'
