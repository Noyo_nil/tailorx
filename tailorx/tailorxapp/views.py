from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from django.contrib.auth.hashers import make_password
from .models import *
from .forms import *
import json

# Create your views here.

#########################
## Buyers & References
#########################

def CategoryWithParts(request):
    if not request.user.is_authenticated:
        return redirect('index')
    else:
        categories = Category.objects.all()
        parts = CategoryParts.objects.all()
        context = {'categories': categories, 'parts': parts}
        return render(request, "categories_with_parts.html", context)

def addCategory(request):
    if not request.user.is_authenticated:
        return redirect('index')
    else:
        if request.method == 'POST':
            form = categoryForm(request.POST)
            if form.is_valid():
                form.save()
                message = 'Category added successfully!'
                messages.success(request, message)
            else:
                for field in form:
                    for error in field.errors:
                        messages.warning(request, "%s : %s" % (field.name, error))
        else:
            message = 'Method is not allowed!'
            messages.warning(request, message)
        return redirect('category_with_parts')

@csrf_exempt
def deleteCategory(request):
    if not request.user.is_authenticated:
        return redirect('index')
    else:
        if request.user.is_superuser:
            if request.method == 'POST':
                category = Category.objects.get(id=request.POST.get('id'))
                category.delete()
                response_data = {}
                response_data['status'] = 'OK'

                return HttpResponse(
                    json.dumps(response_data),
                    content_type="application/json"
                )
        else:
            message = 'You are not authorised!'
            messages.warning(request, message)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

@csrf_exempt
def getCategory(request):
    if not request.user.is_authenticated:
        return redirect('index')
    else:
        if request.method == 'POST':
            category = Category.objects.get(id=request.POST.get('id'))
            response_data = {}
            response_data['name'] = category.name

            return HttpResponse(
                json.dumps(response_data),
                content_type="application/json"
            )

@csrf_exempt
def updateCategory(request):
    if not request.user.is_authenticated:
        return redirect('index')
    else:
        if request.method == 'POST':
            category = Category.objects.get(id=request.POST.get('id', ''))
            form = categoryForm(request.POST, instance=category)
            if form.is_valid():
                form.save()
                message = 'Category Updated Successfully!'
                messages.success(request, message)
            else:
                for field in form:
                    for error in field.errors:
                        messages.warning(request, "%s : %s" % (field.name, error))
        else:
            message = 'Method is not allowed!'
            messages.warning(request, message)
        return redirect('category_with_parts')

def addParts(request):
    if not request.user.is_authenticated:
        return redirect('index')
    else:
        if request.method == 'POST':
            print(request.POST['name'])
            form = categoryPartsForm(request.POST)
            if form.is_valid():
                form.save()
                message = 'Parts added successfully!'
                messages.success(request, message)
            else:
                for field in form:
                    for error in field.errors:
                        messages.warning(request, "%s : %s" % (field.name, error))
        else:
            message = 'You are not allowed!'
            messages.warning(request, message)
        return redirect('category_with_parts')

@csrf_exempt
def deleteParts(request):
    if not request.user.is_authenticated:
        return redirect('index')
    else:
        if request.user.is_superuser:
            if request.method == 'POST':
                ref = CategoryParts.objects.get(id=request.POST.get('id'))
                ref.delete()
                response_data = {}
                response_data['status'] = 'OK'

                return HttpResponse(
                    json.dumps(response_data),
                    content_type="application/json"
                )
        else:
            message = 'You are not authorised!'
            messages.warning(request, message)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

@csrf_exempt
def getParts(request):
    if not request.user.is_authenticated:
        return redirect('index')
    else:
        if request.method == 'POST':
            ref = CategoryParts.objects.get(id=request.POST.get('id'))
            response_data = {}
            response_data['category'] = ref.category.name
            response_data['name'] = ref.name

            return HttpResponse(
                json.dumps(response_data),
                content_type="application/json"
            )

@csrf_exempt
def updateParts(request):
    if not request.user.is_authenticated:
        return redirect('index')
    else:
        if request.method == 'POST':
            ref = CategoryParts.objects.get(id=request.POST.get('id', ''))
            form = categoryPartsForm(request.POST, instance=ref)
            if form.is_valid():
                form.save()
                message = 'Reference Updated Successfully!'
                messages.success(request, message)
            else:
                for field in form:
                    for error in field.errors:
                        messages.warning(request, "%s : %s" % (field.name, error))
        else:
            message = 'Method is not allowed!'
            messages.warning(request, message)
        return redirect('category_with_parts')

######## Order and Challans Portion #########
def addOrder(request):
    if not request.user.is_authenticated:
        return redirect('index')
    else:
        categories = Category.objects.all()
        parts = CategoryParts.objects.all()
        last_order = Order.objects.count()
        context = {'categories': categories, 'parts': parts,'order_no':last_order+1}
        return render(request, "order/form.html", context)

@csrf_exempt
def getCategoryWiseParts(request):
    if not request.user.is_authenticated:
        return redirect('index')
    else:
        print(request.POST.get('category'))
        data = CategoryParts.objects.filter(category=request.POST.get('category'))
        print(data)
        result = []
        for i in data:
            details = {
                'id':i.id,
                'name':i.name,
            }
            result.append(details)

        return HttpResponse(
                json.dumps(result),
                content_type="application/json"
            )

def saveOrder(request):
    if not request.user.is_authenticated:
        return redirect('index')
    else:
        if request.method == 'POST':
            request.POST = request.POST.copy()
            print(request.POST['category'])
            customer = customerForm(request.POST)
            if customer.is_valid():
                cs = customer.save()
                order = Order(order_no=request.POST['order_no'],order_qty=request.POST['order_qty'],order_date=request.POST['order_date'],delivery_date=request.POST['delivery_date'],customer_id=cs.id,order_taken_by_id=request.user.id)
                order.save()
                category = request.POST['category']
                order.order_category.set(category)

                # password = make_password(123456, salt=None, hasher='default')
                # customer = User(name=request.POST['customer'],email=request.POST['customer_email'],phone=request.POST['customer_phone'],password=password)
                # customer.save()

                parts = request.POST.getlist('parts')
                print(parts)
                measurement_number = request.POST.getlist('measurement_number')
                zipped = zip(parts, measurement_number)

                for parts,measurement_number in zipped:
                    data = {'order_id': order.id,
                            'category_id': category,
                            'parts': CategoryParts.objects.get(name=parts),
                            'measurement_number': measurement_number
                            }

                    pdForm = measurementForm(data)
                count = Challan.objects.all().count()
                if count < 10:
                    challan_no = 'x' + '0' + count
                else:
                    challan_no = 'x' + count
                challan = Challan(order_id=order.id,quantity=request.POST['order_qty'],amount=request.POST['amount'],status=0,challan_no=challan_no)
                challan.save()
                return redirect('challan_details', challan.id)
                message = 'Order added successfully!'
                messages.success(request, message)
            else:
                for field in form:
                    for error in field.errors:
                        messages.warning(request, "%s : %s" % (field.name, error))
        else:
            message = 'You are not allowed!'
            messages.warning(request, message)
        return redirect('order_list')

def orderList(request):
    if not request.user.is_authenticated:
        return redirect('index')
    else:
        orders = Order.objects.all()
        context = {
            'orders':orders,
        }
        return render(request,'order/order_list.html',context)

def editOrder(request,id):
    if not request.user.is_authenticated:
        return redirect('index')
    else:
        return redirect('order_list')

@csrf_exempt
def deleteOrder(request):
    if not request.user.is_authenticated:
        return redirect('index')
    else:
        if request.user.is_superuser:
            if request.method == 'POST':
                challan = Challan.objects.get(order_id=request.POST.get('id'))
                challan.delete()
                measurement = Measurement.objects.filter(order_id=request.POST.get('id'))
                for ms in measurement:
                    ms.delete()
                po = Order.objects.get(id=request.POST.get('id'))
                po.delete()
                response_data = {}
                response_data['status'] = 'OK'

                return HttpResponse(
                    json.dumps(response_data),
                    content_type="application/json"
                )
        else:
            message = 'You are not authorised!'
            messages.warning(request, message)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def challanDetails(request, id):
    if not request.user.is_authenticated:
        return redirect('index')
    else:
        challan = Challan.objects.get(id=id)

        context = {'challan': challan,}
        return render(request, "challan/details.html", context)

def challanList(request):
    if not request.user.is_authenticated:
        return redirect('index')
    else:
        challans = Challan.objects.filter(status=0)

        context = {'challans':challans}
        return render(request,"challan/challan_list.html",context)