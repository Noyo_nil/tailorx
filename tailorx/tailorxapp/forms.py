from django import forms
from .models import *

class categoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = "__all__"

class categoryPartsForm(forms.ModelForm):
    class Meta:
        model = CategoryParts
        fields = "__all__"

class orderForm(forms.ModelForm):
	class Meta:
		model = Order
		fields = "__all__"

class measurementForm(forms.ModelForm):
	class Meta:
		model = Measurement
		fields = ('order','category','parts','measurement_number')


class customerForm(forms.ModelForm):
	class Meta:
		model = Customer
		fields = "__all__"

	def __init__(self, *args, **kwargs):
	    super(customerForm, self).__init__(*args, **kwargs)
	    self.fields['address'].required = False
	    self.fields['email'].required = False
	    self.fields['contact'].required = False