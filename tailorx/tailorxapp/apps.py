from django.apps import AppConfig


class TailorxappConfig(AppConfig):
    name = 'tailorxapp'
