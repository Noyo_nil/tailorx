# Generated by Django 2.2.2 on 2020-02-03 19:01

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('tailorxapp', '0002_auto_20200201_1900'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='order_qty',
            field=models.IntegerField(blank=True, default=0),
            preserve_default=False,
        ),
    ]
