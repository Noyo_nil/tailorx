from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    # Category & Parts
    path('category-parts', views.CategoryWithParts, name='category_with_parts'),
    path('add_category', views.addCategory, name='add_category'),
    path('get_category', views.getCategory, name='get_category'),
    path('update_category', views.updateCategory, name='update_category'),
    path('delete_category', views.deleteCategory, name='delete_category'),

    path('add_parts', views.addParts, name='add_parts'),
    path('get_parts', views.getParts, name='get_parts'),
    path('update_parts', views.updateParts, name='update_parts'),
    path('delete_parts', views.deleteParts, name='delete_parts'),

    # Orders
    path('add-order', views.addOrder, name='add_order'),
    path('save-order', views.saveOrder, name='save_order'),
    path('order-list', views.orderList, name='order_list'),
    path('edit-order/<id>', views.editOrder, name='edit_order'),
    path('delete-order', views.deleteOrder, name='delete_order'),
    # Challan
    path('challan-details/<int:id>', views.challanDetails, name='challan_details'),
    path('challan-list', views.challanList, name='challan_list'),

    path('ajax-get-category-parts',views.getCategoryWiseParts, name='ajax_get_category_wise_parts')
]