from django.db import models
from django.utils import timezone
from user.models import User
from decimal import Decimal

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length = 50, null=True)
    created_at = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    class Meta:
        db_table = 'categories'

class CategoryParts(models.Model):
    name = models.CharField(max_length = 50, null=True)
    category = models.ForeignKey(Category,blank=True,null=True,on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    class Meta:
        db_table = 'categorie_parts'

class Customer(models.Model):
	name = models.CharField(max_length=60, blank=True, null=True)
	address = models.CharField(max_length=100, blank=True, null=True)
	email = models.EmailField(unique=True)
	contact = models.CharField(max_length=20, blank=True, null=True)
	added_by = models.ForeignKey(User, blank=True, null=True,on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'customers'

class Order(models.Model):
	order_no = models.CharField(max_length=20,blank=True,null=True)
	order_qty = models.IntegerField(blank=True)
	order_date = models.DateField(blank=True,null=True)
	delivery_date = models.DateField(blank=True,null=True)
	order_category = models.ManyToManyField(Category)
	customer = models.ForeignKey(Customer,blank=True,null=True,on_delete=models.CASCADE,related_name='order_for')
	order_taken_by = models.ForeignKey(User,blank=True,null=True,on_delete=models.CASCADE)
	created_at = models.DateField(auto_now_add=True,blank=True,null=True)
	
	class Meta:
		db_table = 'orders'		

class Measurement(models.Model):
	order = models.ForeignKey(Order,blank=True,null=True,on_delete=models.CASCADE)
	category = models.ForeignKey(Category,blank=True,null=True,on_delete=models.CASCADE)
	parts = models.ForeignKey(CategoryParts,blank=True,null=True,on_delete=models.CASCADE)
	measurement_number = models.DecimalField(max_digits=12, decimal_places=2, default=Decimal(0.00), null=True, blank=True)
	created_at = models.DateField(auto_now_add=True,blank=True,null=True)

	class Meta:
		db_table = 'measurements'


class Challan(models.Model):
	challan_no = models.CharField(max_length=45,blank=True,null=True)
	order = models.ForeignKey(Order,blank=True,null=True,on_delete=models.CASCADE)
	quantity = models.IntegerField(blank=True,null=True)
	amount = models.DecimalField(max_digits=12, decimal_places=2, default=Decimal(0.00), null=True, blank=True)
	challan_date = models.DateField(auto_now_add=True,blank=True,null=True)
	created_at = models.DateField(auto_now_add=True,blank=True,null=True)
	status = models.BooleanField(default=False)

	class Meta:
		db_table = 'challans'